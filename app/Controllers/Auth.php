<?php
namespace App\Controllers;

use Google\Client as GoogleClient;

class Auth extends BaseController
{
    // Protected properties can be accessed within the class and by classes derived from that class
    protected $client;   // Will hold the instance of the Google Client
    protected $base_url; // Will hold the base URL of the app from the environment variables

    /**
     * Class constructor.
     */
    public function __construct()
    {
        // Store the base URL in the class property from the .env file
        $this->base_url = getenv('app.baseURL');

        helper('url'); 

        $this->session = session();

        // Instantiate the Google Client and set it up with the necessary details
        $this->client = new GoogleClient();
        $this->client->setClientId(getenv('GOOGLE_CLIENT_ID'));        // Set the Google Client ID from .env
        $this->client->setClientSecret(getenv('GOOGLE_CLIENT_SECRET')); // Set the Google Client Secret from .env
        $this->client->setRedirectUri($this->base_url . 'login/callback'); // Set the Redirect URI for Google OAuth callback
        $this->client->addScope("email");   // Request access to user's email
        $this->client->addScope("profile"); // Request access to user's basic profile info


    }

    /**
     * Initiates Google OAuth login process.
     */
    public function google_login()
    {
        // Get the authentication URL from the Google Client
        $authUrl = $this->client->createAuthUrl();
        // Redirect the user to Google's OAuth server
        return redirect()->to($authUrl);
    }

    /**
     * Google OAuth callback function.
     */
    public function google_callback()
    {
        // Fetch the access token using the authorization code received from Google
        $token = $this->client->fetchAccessTokenWithAuthCode($this->request->getGet('code'));
        // Set the access token to the Google Client
        $this->client->setAccessToken($token);

        // Create a new Google Oauth2 service instance
        $google_oauth = new \Google\Service\Oauth2($this->client);
        // Get user information from Google
        $google_account_info = $google_oauth->userinfo->get();

        // Load the user model to interact with the database
        $usersModel = new \App\Models\UsersModel();
        // Check if the user's email exists in the database
        $user = $usersModel->where('email', $google_account_info->email)->first();

        if (!$user) {
            // If the user doesn't exist, create a new user with the info from Google
            $newData = [
                'email' => $google_account_info->email,
                'first_name' => $google_account_info->given_name,
                'last_name' => $google_account_info->family_name,
                'role_id' => 2,
            ];
            $usersModel->insert($newData);
            // Retrieve the newly created user info
            $user = $usersModel->where('email', $google_account_info->email)->first();
        }

        $restaurantsModel = new \App\Models\RestaurantsModel();
        $resto = $restaurantsModel->where('restaurant_id', $user['restaurant_id'])->first();

        // Set user information in the session
        session()->set([
            'isLoggedIn' => true,
                    'user_id' => $user['user_id'],
                    'email' => $user['email'],
                    'role_id' => $user['role_id'],
                    'first_name' => $user['first_name'], 
                    'last_name' => $user['last_name'],
                    'image_path' => $user['image_path'],
                    'resto' => isset($resto) ? $resto['name'] : '',
                    'resto_id' => isset($resto) ? $resto['restaurant_id'] : '',
                    'resto_path' =>  isset($resto) ? $resto['image_path'] : ''
        ]);

        // Redirect user to the admin dashboard if they are an admin, otherwise to their resume page
        if (session()->get('role_id') == 1) {
            return redirect()->to('/admin');
        } else {
            return redirect()->to('/dashboard');
        }
    }

    public function login() 
    {
        if ($this->request->getMethod() === 'POST') {
            $data = $this->request->getPost();
            $email = $data['email'];
            $password_hash = password_hash($data['password'], PASSWORD_DEFAULT);;

            $usersModel = new \App\Models\UsersModel();
            $user = $usersModel->where('email', $email)->first();

            if (!$user) {
                $this->session->setFlashdata('error', 'Invalid Email.');
                return redirect()->to('/login');
            }

            if (password_verify($data['password'], $user['password_hash'])) {
                $this->session->setFlashdata('success', 'Login Success.');

                $restaurantsModel = new \App\Models\RestaurantsModel();
                $resto = $restaurantsModel->where('restaurant_id', $user['restaurant_id'])->first();

                session()->set([
                    'isLoggedIn' => true,
                    'user_id' => $user['user_id'],
                    'email' => $user['email'],
                    'role_id' => $user['role_id'],
                    'first_name' => $user['first_name'], 
                    'last_name' => $user['last_name'],
                    'image_path' => $user['image_path'],
                    'resto' => isset($resto) ? $resto['name'] : '',
                    'resto_id' => isset($resto) ? $resto['restaurant_id'] : '',
                    'resto_path' =>  isset($resto) ? $resto['image_path'] : ''
                ]);

                if (session()->get('role_id') == 1) {
                    return redirect()->to('/admin');
                } else {
                    return redirect()->to('/dashboard');
                }
            } else {
                $this->session->setFlashdata('error', 'Invalid Password.');
                return redirect()->to('/login');
            }
        }
        

        return view('desktop/login/login');
    }


    /**
     * Logs the user out by clearing session data.
     */
    public function logout()
    {
        // Access the session service
        $session = session();

        // Remove specific session variables
        $session->remove(['isLoggedIn', 'user_id', 'email', 'role_id', 'first_name', 'last_name', 'image_path', 'resto', 'resto_id', 'resto_path']);

        // Redirect the user to the homepage
        return redirect()->to('/');
    }
}