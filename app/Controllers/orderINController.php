<?php
namespace App\Controllers;

use chillerlan\QRCode\{QRCode, QROptions};


use CodeIgniter\Controller;

class orderINController extends BaseController
{
    public function __construct()
    {
        // Load the URL helper, it will be useful in the next steps
        // Adding this within the __construct() function will make it 
        // available to all views in the Controller
        helper('url');

        $this->session = session();
    }

    public function index(): string
    {
        return view('desktop/login/landing_page');
    }

    public function register()
    {
        $restaurantsModel = new \App\Models\RestaurantsModel();
        $usersModel = new \App\Models\UsersModel();

        $data['restaurants'] = $restaurantsModel->findAll();

        if ($this->request->getMethod() === 'POST') {
            $data = $this->request->getPost();

            if ($data['password'] != $data['confirm-password']) {
                $this->session->setFlashdata('error', 'Passwords do not match.');
                return redirect()->to('/register');
            }
            
            $newData = [
                'email' => $data['email'],
                'password_hash' => password_hash($data['password'], PASSWORD_DEFAULT),
                'restaurant_id' => $data['restaurant_id'],
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],
                'phone' => $data['phone'],
                'role_id' => 3,
                'image_path' => 'users/default.jpeg',
            ];

            $usersModel->insert($newData);

            $user = $usersModel->where('email', $data['email'])->first();

            $resto = $restaurantsModel->where('restaurant_id', $user['restaurant_id'])->first();

            session()->set([
                'isLoggedIn' => true,
                'user_id' => $user['user_id'],
                'email' => $user['email'],
                'role_id' => $user['role_id'],
                'first_name' => $user['first_name'], 
                'last_name' => $user['last_name'],
                'image_path' => $user['image_path'],
                'resto' => isset($resto) ? $resto['name'] : '',
                'resto_id' => isset($resto) ? $resto['restaurant_id'] : '',
                'resto_path' =>  isset($resto) ? $resto['image_path'] : ''
            ]);

            $this->session->setFlashdata('success', 'User successfully created!');
            return redirect()->to('/dashboard');
        }
        
        return view('desktop/login/register', $data);
    }

    public function register_restaurant()
    {

        $restaurantsModel = new \App\Models\RestaurantsModel();
        $usersModel = new \App\Models\UsersModel();
        $tablesModel = new \App\Models\TablesModel();

        $data['restaurants'] = $restaurantsModel->findAll();

        if ($this->request->getMethod() === 'POST') {
            $data = $this->request->getPost();

            if ($data['password'] != $data['confirm-password']) {
                $this->session->setFlashdata('error', 'Passwords do not match.');
                return redirect()->to('/register-restaurant');
            }
            
            $newDataResto = [
                'name' => $data['name'],
                'description' => $data['description'],
                'table_num' => $data['table_num'],
                'image_path' => 'users/default.jpeg',
            ];

            $resto_id = $restaurantsModel->insert($newDataResto);

            for ($i = 1; $i < $data['table_num'] + 1; $i++) {
                $tablesModel->insert([
                    'table_id' => $i,
                    'restaurant_id' => $resto_id,
                    'status' => 'inactive',
                    'price' => 0,
                ], );
            }
            
            $newData = [
                'email' => $data['email'],
                'password_hash' => password_hash($data['password'], PASSWORD_DEFAULT),
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],
                'phone' => $data['phone'],
                'role_id' => 2,
                'image_path' => 'users/default.jpeg',
                'restaurant_id' => $resto_id
            ];

            $usersModel->insert($newData);

            

            $user = $usersModel->where('email', $data['email'])->first();
            $resto = $restaurantsModel->where('restaurant_id', $resto_id)->first();

            session()->set([
                'isLoggedIn' => true,
                'user_id' => $user['user_id'],
                'email' => $user['email'],
                'role_id' => $user['role_id'],
                'first_name' => $user['first_name'], 
                'last_name' => $user['last_name'],
                'image_path' => $user['image_path'],
                'resto' => isset($resto) ? $resto['name'] : '',
                'resto_id' => isset($resto) ? $resto['restaurant_id'] : '',
                'resto_path' =>  isset($resto) ? $resto['image_path'] : ''
            ]);

            $this->session->setFlashdata('success', 'User successfully created!');
            return redirect()->to('/dashboard');
        }
        
        return view('desktop/login/register_restaurant', $data);
    }

    public function dashboard(): string
    {
        $data['title'] = 'Dashboard';

        $ordersModel = new \App\Models\OrdersModel();
        $orderItemsModel = new \App\Models\OrderItemsModel();
        $tablesModel = new \App\Models\TablesModel();

        $data['incoming'] = $ordersModel->where('restaurant_id', session()->get('resto_id'))->where('status', 'incoming')->findAll();
        $data['orderItems'] = $orderItemsModel->join('MenuItems', 'OrderItems.menu_item_id = MenuItems.menu_item_id')->where('restaurant_id', session()->get('resto_id'))->findAll();
        $data['tables'] = $tablesModel->where('restaurant_id', session()->get('resto_id'))->findAll();
        $data['tableOrders'] = $tablesModel->join('Orders', 'Tables.table_id = Orders.table_id AND Tables.restaurant_id = Orders.table_id', 'left')->findAll();
        


        return view('desktop/dashboard', $data);
    }

    public function tables(): string
    {
        $data['title'] = 'Tables';

        $tablesModel = new \App\Models\TablesModel();
        $ordersModel = new \App\Models\OrdersModel();
        $orderItemsModel = new \App\Models\OrderItemsModel();

        $data['tables'] = $tablesModel->where('restaurant_id', session()->get('resto_id'))->findAll();
        $data['tableOrders'] = $tablesModel->join('Orders', 'Tables.table_id = Orders.table_id AND Tables.restaurant_id = Orders.table_id', 'left')->findAll();
        $data['orderItems'] = $orderItemsModel->join('MenuItems', 'OrderItems.menu_item_id = MenuItems.menu_item_id')->where('restaurant_id', session()->get('resto_id'))->findAll();

        return view('desktop/tables', $data);
    }

    public function table_details($id): string
    {
        $data['title'] = 'Table ' . sprintf('%02d', $id);

        $tablesModel = new \App\Models\TablesModel();
        $ordersModel = new \App\Models\OrdersModel();
        $orderItemsModel = new \App\Models\OrderItemsModel();

        $data['id'] = $id;

        $qrlink = base_url('order/' . session()->get('resto_id') . '/' . $id);
        $data['qr'] = (new QRCode)->render($qrlink);


        $data['table'] = $tablesModel->where('table_id', $id)->first();
        $data['orderItems'] = $orderItemsModel->join('MenuItems', 'OrderItems.menu_item_id = MenuItems.menu_item_id')->where('restaurant_id', session()->get('resto_id'))->findAll();

        return view('desktop/table_details', $data);
    }

    public function order($restaurant_id, $table_id): string 
    {
        $data['title'] = 'Order';

        $menuSectionsModel = new \App\Models\MenuSectionsModel();
        $menuItemsModel = new \App\Models\MenuItemsModel();
        $restaurantsModel = new \App\Models\RestaurantsModel();
        
        $data['table_id'] = $table_id;
        

        $data['restaurant'] = $restaurantsModel->where('restaurant_id', $restaurant_id)->first();
        $data['sections'] = $menuSectionsModel->join('Menus', 'MenuSections.menu_id = Menus.menu_id')->where('restaurant_id', $restaurant_id)->findAll();
        $data['items'] = $menuItemsModel->join('Menus', 'MenuItems.menu_id = Menus.menu_id')->where('restaurant_id', $restaurant_id)->findAll();

        return view('mobile/order', $data);
    }

    public function orders(): string
    {
        $data['title'] = 'Orders';

        $ordersModel = new \App\Models\OrdersModel();
        $orderItemsModel = new \App\Models\OrderItemsModel();


        $data['orders'] = $ordersModel->where('restaurant_id', session()->get('resto_id'))->findAll();
        $data['incoming'] = $ordersModel->where('restaurant_id', session()->get('resto_id'))->where('status', 'incoming')->findAll();
        $data['processing'] = $ordersModel->where('restaurant_id', session()->get('resto_id'))->where('status', 'processing')->findAll();
        $data['completed'] = $ordersModel->where('restaurant_id', session()->get('resto_id'))->where('status', 'completed')->findAll();
        $data['orderItems'] = $orderItemsModel->join('MenuItems', 'OrderItems.menu_item_id = MenuItems.menu_item_id')->where('restaurant_id', session()->get('resto_id'))->findAll();

        return view('desktop/orders', $data);
    }

    public function order_details($id): string
    {
        $data['title'] = 'Order #' . sprintf('%03d', $id);

        $ordersModel = new \App\Models\OrdersModel();
        $orderItemsModel = new \App\Models\OrderItemsModel();

        $data['id'] = $id;

        $data['order'] = $ordersModel->where('order_id', $id)->first();
        $data['orderItems'] = $orderItemsModel->join('MenuItems', 'OrderItems.menu_item_id = MenuItems.menu_item_id')->where('restaurant_id', session()->get('resto_id'))->findAll();

        return view('desktop/order_details', $data);
    }

    public function menu(): string
    {
        $data['title'] = 'Menu';
        return view('desktop/menu', $data);
    }

    public function settings(): string
    {
        $data['title'] = 'Settings';
        return view('desktop/settings', $data);
    }

    public function profile($user_id): string
    {
        $data['title'] = 'Profile';

        $usersModel = new \App\Models\UsersModel();

        $data['user'] = $usersModel->where('user_id', $user_id)->findAll();

        if (!$data['user']) {
            throw new \CodeIgniter\Exceptions\PageNotFoundException('User Not Found');
        }

        return view('desktop/profile', $data);
    }

    public function restaurant(): string
    {
        $data['title'] = 'Restaurant';
        return view('desktop/manage/restaurant', $data);
    }

    public function staff(): string
    {
        $data['title'] = 'Staff';
        return view('desktop/manage/staff', $data);
    }

    public function admin(): string
    {
        $data['title'] = 'Admin Dashboard';

        $usersModel = new \App\Models\UsersModel();
        $restaurantsModel = new \App\Models\RestaurantsModel();

        $data['users'] = $usersModel->join('Roles', 'Users.role_id = Roles.role_id')->orderBy('user_id', 'ASC')->findAll();
        $data['restaurants'] = $restaurantsModel->findAll();
        $data['userManagers'] = $usersModel->where('role_id = 2')->orderBy('user_id', 'ASC')->findAll();
        $data['userStaff'] = $usersModel->where('role_id = 3')->orderBy('user_id', 'ASC')->findAll();

        return view('desktop/admin/dashboard', $data);
    }

    public function admin_restaurants(): string
    {
        $data['title'] = 'Restaurants';
        $usersModel = new \App\Models\UsersModel();
        $restaurantsModel = new \App\Models\RestaurantsModel();

        $data['restaurants'] = $restaurantsModel->findAll();



        return view('desktop/admin/restaurants', $data);
    }

    public function admin_users(): string
    {
        $data['title'] = 'Users';
        $usersModel = new \App\Models\UsersModel();
        $restaurantsModel = new \App\Models\RestaurantsModel();

        $data['users'] = $usersModel->join('Roles', 'Users.role_id = Roles.role_id')->orderBy('user_id', 'ASC')->findAll();
        $data['restaurants'] = $restaurantsModel->findAll();
        $data['userManagers'] = $usersModel->where('role_id = 2')->orderBy('user_id', 'ASC')->findAll();
        $data['userStaff'] = $usersModel->where('role_id = 3')->orderBy('user_id', 'ASC')->findAll();

        return view('desktop/admin/users', $data);
    }

    public function admin_profile(): string
    {
        $data['title'] = 'Profile';
        return view('desktop/admin/profile', $data);
    }
}