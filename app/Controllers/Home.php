<?php

namespace App\Controllers;

class Home extends BaseController
{
    public function index(): string
    {
        return view('desktop/login/landing_page');
    }

    public function dashboard(): string
    {
        return view('desktop/dashboard');
    }

    public function menu(): string
    {
        return view('menu');
    }

    public function tables(): string
    {
        return view('tables');
    }

    public function order_page(): string
    {
        return view('order_page');
    }
}
