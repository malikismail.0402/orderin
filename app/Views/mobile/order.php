<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>orderIN - <?= esc($title) ?></title>
  <!-- This is the main stylesheet for Bootstrap. It includes all the CSS necessary for Bootstrap's components and utilities to work. -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
  <!-- Include Bootstrap Icons -->
  <!-- This link imports the Bootstrap Icons library, which provides a wide range of SVG icons for use in your projects. -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Jost:ital,wght@0,100..900;1,100..900&display=swap"
    rel="stylesheet">
</head>

<body class="container-fluid d-flex p-0 flex-column bg-body" style="
  background-color: #f8f9fa; font-family: 'Jost'; sans-serif !important; 
  ">
  <style>
    .btn-light {
      --bs-btn-bg: white;
      --bs-btn-hover-bg: #E2C691;
      --bs-btn-hover-color: #1E5A7C;
      --bs-btn-active-bg: #E2C691;
      --bs-btn-active-color: #1E5A7C;
      --bs-btn-active-border-color: #1E5A7C;
      --bs-btn-hover-border-color: #1E5A7C;
      border-color: #1E5A7C;
    }

    .btn-primary {
      --bs-btn-bg: #1E5A7C;
      --bs-btn-hover-bg: #E2C691;
      --bs-btn-hover-color: #1E5A7C;
      --bs-btn-active-bg: #E2C691;
      --bs-btn-active-color: #1E5A7C;
      --bs-btn-active-border-color: #E2C691;
      --bs-btn-hover-border-color: #1E5A7C;
      border-color: white;
    }

    .btn-secondary {
      color: #1E5A7C;
      --bs-btn-bg: #E2C691;
      --bs-btn-hover-bg: #1E5A7C;
      --bs-btn-hover-color: white;
      --bs-btn-active-bg: #1E5A7C;
      --bs-btn-active-color: white;
      --bs-btn-active-border-color: #1E5A7C;
      --bs-btn-hover-border-color: #E2C691;
      border-color: white;
    }

    .btn-danger {
      color: #dc3545;
      --bs-btn-bg: white;
      --bs-btn-hover-color: white;
      --bs-btn-active-color: white;
      border-color: white;
    }

    .text-bg-primary {
      background-color: #1E5A7C !important;
      color: #E2C691 !important;
    }

    .text-bg-secondary {
      background-color: #E2C691 !important;
      color: #1E5A7C !important;
    }

    .text-primary {
      color: #1E5A7C !important;
    }

    nav::-webkit-scrollbar {
        display: none; /* for Chrome, Safari, and Opera */
    }
  </style>

    <div class="w-100 d-flex flex-column">
        <div class="w-100 position-absolute text-bg-primary" style="height: 205px; border-bottom-width: 5px; border-bottom-style: solid; border-bottom-color: #E2C691">

        </div>

        <div class="d-flex position-relative justify-content-between" style="margin-top: 150px;">
            <img class="img-fluid bg-white object-fit-contain ms-4" src='<?= base_url($restaurant['image_path']) ?>' 
            style="width: 100px; height: 100px; border-radius: 50px; border-width: 3px; border-style: solid; border-color: #E2C691">
            
            <div class='d-flex align-items-end'>
                <p class='mb-0 me-4'><span class="badge text-bg-secondary ms-2">Table <?= sprintf('%02d', esc($table_id)) ?></span></p>

            </div>

        </div>
        
        <div class="mx-2 px-4 py-3">
            <h3><?= esc($restaurant['name']) ?></h3>
            <p class='mb-0' style='font-size: 13px; color: #6c757d'><?= esc($restaurant['description']) ?></p>
        </div>



    </div>
    <nav class="navbar bg-white sticky-top py-0 d-flex text-nowrap flex-nowrap border-bottom" style="overflow-x: auto;  ">
        <button class="btn btn-light rounded-0 border-0 py-2 w-auto" style="min-width: 85px;" role="button">All</button>
        <?php foreach ($sections as $section): ?>
            <button class="btn btn-light rounded-0 border-0 py-2 text-center" style="min-width: 85px;" role="button"><?= esc($section['name']) ?></button>
        <?php endforeach ?>
    </nav>

    <main>
        <div class="w-100">
            <h4 class='mx-4 mt-3 mb-0 pb-3 border-bottom'>Promotions</h4>
            <?php foreach ($items as $item): ?>
                <div class='d-flex border-bottom mx-4 mt-2 pb-2 align-items-center'>
                    <div class=''>
                        <img class="bg-white object-fit-cover rounded-3" src='<?= base_url($item['image_path']) ?>' 
                        style="width: 80px; height: 80px; border-width: 3px; border-style: solid; border-color: #E2C691">
                    </div>
                    
                    <div class='d-flex flex-column w-100 px-3 py-2'>
                        <div class=''>
                            <p class='mb-0 fw-bold'><?= esc($item['name']) ?></p>
                            <p class='mb-0' style='font-size: 13px; color: #6c757d'><?= esc($item['description']) ?></p>
                        </div>
                        <div class='d-flex justify-content-between align-items-center'>
                            <p class='mb-0'>$<?= number_format((float) esc($item['price']), 2, '.', '')  ?></p>

                            <div class='d-flex align-items-center'>
                                <p class='mb-0 me-2'>- 1 +</p> 
                                <button type="button" class="btn btn-primary d-flex align-items-center border-0 rounded-5 my-1" style='font-size: 13px'>Add</button>
                            </div>
                        </div>
                        
                    </div>
                </div>
            <?php endforeach ?>
            <?php foreach ($sections as $section): ?>
                <h4 class='mx-4 mt-3 mb-0 pb-3 border-bottom'><?= esc($section['name']) ?></h4>
                <?php foreach ($items as $item): ?>
                    <?php if ($section['menu_section_id'] == $item['menu_section_id']): ?>
                        <div class='d-flex border-bottom mx-4 mt-2 pb-2 align-items-center'>
                    <div class=''>
                        <img class="bg-white object-fit-cover rounded-3" src='<?= base_url($item['image_path']) ?>' 
                        style="width: 80px; height: 80px; border-width: 3px; border-style: solid; border-color: #E2C691">
                    </div>
                    
                    <div class='d-flex flex-column w-100 px-3 py-2'>
                        <div class=''>
                            <p class='mb-0 fw-bold'><?= esc($item['name']) ?></p>
                            <p class='mb-0' style='font-size: 13px; color: #6c757d'><?= esc($item['description']) ?></p>
                        </div>
                        <div class='d-flex justify-content-between align-items-center'>
                            <p class='mb-0'>$<?= number_format((float) esc($item['price']), 2, '.', '')  ?></p>

                            <div class='d-flex align-items-center'>
                                <p class='mb-0 me-2'>- 1 +</p> 
                                <button type="button" class="btn btn-primary d-flex align-items-center border-0 rounded-5 my-1" style='font-size: 13px'>Add</button>
                            </div>
                        </div>
                        
                    </div>
                </div>
                    <?php endif ?>
                <?php endforeach ?>
            <?php endforeach ?>
        </div>
    </main>

  <!-- This script includes all of Bootstrap's JavaScript-based components and behaviors, such as modal windows, dropdowns, and tooltips.  -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz"
    crossorigin="anonymous"></script>
</body>

</html>