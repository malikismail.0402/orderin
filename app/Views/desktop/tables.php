<?= $this->extend('desktop/template') ?>
<?= $this->section('content') ?>
<!-- Insert landing page content here -->
<div class="container d-flex flex-column w-100 px-5 pt-3">
    <div class="bg-white rounded-5 py-4 shadow w-100">
        <div class="d-flex justify-content-between align-items-center px-5 mb-3">
            <h3 class="mb-0">All Tables</h3>
            <button type="button" class="btn btn-primary d-flex align-items-center pe-3 border-0">
                <i class="bi bi-pencil d-flex align-items-center me-2"></i>Edit Tables</button>
        </div>
        <div class="d-flex rounded-5 mx-4 mt-4 p-4 w-auto flex-wrap" style="background-color: #f8f9fa;">
            <?php foreach ($tables as $table): ?>
                <a class="<?= $table['status'] == 'active' ? 'active' : '' ?> d-flex rounded-5 mx-2 my-2 py-4 border-0 justify-content-center align-items-center btn btn-light shadow"
                    style="width: 235px;" role="button" href='<?= base_url('tables/' . $table['table_id']); ?>'>
                    <div class="w-50" style="border-right-width: 2px; border-right-style: solid;">
                        <h5>T<?= sprintf('%02d', esc($table['table_id'])) ?></h5>
                        <?php if ($table['status'] == 'active'): ?>
                            <span class="badge text-bg-success">Active</span>
                        <?php else: ?>
                            <span class="badge text-bg-danger">Inactive</span>
                        <?php endif; ?>
                        <div class="icon-link icon-link-hover link-underline link-underline-opacity-0 ms-2 mt-2"
                            style="font-size: 14px">
                            Details
                            <i class="bi bi-arrow-right-short d-flex align-items-center"></i>
                        </div>
                    </div>
                    <div class="w-50">
                        <div class="text-start mb-0 d-block text-truncate align-top" style="height: 80px">
                            <?php foreach($tableOrders as $order): ?>

                                <?php if ($order['table_id'] == $table['table_id'] 
                                and $order['status'] != 'completed'
                                ): ?>
                                    <div class="ms-2 ps-1 me-2 mb-2" style="border-left-width: 1px; border-left-style: solid;">
                                        <?php foreach($orderItems as $item): ?>
                                            <?php if ($item['order_id'] == $order['order_id']): ?>
                                                <div class="d-flex justify-content-between" style="font-size: 10px;">
                                                    <p class="mb-0"><?= esc($item['name']) ?></p>
                                                    <p class='mb-0'>x <?= esc($item['amount']) ?></p>
                                                </div>
                                            <?php endif ?>
                                        <?php endforeach ?>
                                    </div>
                                <?php endif ?>
                            <?php endforeach ?>
                            <?php if($table['status'] != 'active'): ?>
                                    <p class="ms-2 ps-1 me-2 mb-2" style="font-size: 10px">No Orders</p>
                            <?php endif ?>
                        </div>
                    </div>
                </a>
            <?php endforeach ?>
        </div>
    </div>

    <div class="bg-white rounded-5 my-5 py-4 shadow w-100">
        <div class="d-flex justify-content-between align-items-center px-5 mb-3">
            <h3 class="mb-0">All Tables</h3>
            <button type="button" class="btn btn-primary d-flex align-items-center pe-3 border-0">
                <i class="bi bi-pencil d-flex align-items-center me-2"></i>Edit Tables</button>
        </div>
        <table class="table table-lg">
            <thead>
                <tr>
                    <th class="ps-4 text-center" scope="col">Table No.</th>
                    <th scope="col">Status</th>
                    <th scope="col">Last Active</th>
                    <th class="pe-5" scope="col"></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($tables as $table): ?>
                    <tr class="align-middle my-3">
                        <td class="text-center">T<?= sprintf('%02d', esc($table['table_id'])) ?></td>
                        <td><?= esc($table['status']) ?></td>
                        <td><?= esc($table['updated_at']) ?></td>
                        <td class="dropdown">
                            <a href="" class="btn border-0" role="button" data-bs-toggle="dropdown">
                                <i class="bi bi-three-dots-vertical"></i>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a class="dropdown-item btn btn-light d-flex" href="#">
                                        <i class="bi bi-info d-flex align-items-center me-2"></i>Details
                                    </a>
                                </li>
                                <li>
                                    <a class="dropdown-item btn btn-light d-flex" href="#">
                                        <i class="bi bi-pencil d-flex align-items-center me-2"></i>Edit
                                    </a>
                                </li>
                                <li>
                                    <a class="dropdown-item btn btn-danger text-danger d-flex" href="#">
                                        <i class="bi bi-trash d-flex align-items-center me-2"></i>Delete
                                    </a>
                                </li>
                            </ul>
                        </td>
                    </tr>

                <?php endforeach ?>

            </tbody>
        </table>
    </div>

</div>

<?= $this->endSection() ?>