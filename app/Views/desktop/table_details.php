<?= $this->extend('desktop/template') ?>
<?= $this->section('content') ?>
<!-- Insert landing page content here -->
<div class="container d-flex w-100 px-5 pt-3 justify-content-center">
    <div class="d-flex bg-body flex-column rounded-4 align-items-center overflow-hidden shadow h-100 me-4"
        style="min-height: 550px; width: 50%">
        <div class='d-flex align-items-center justify-content-between w-100 text-bg-primary px-4 pt-2 pb-1'>
            <h3 class='mb-0'>Items</h3>
            <?= esc($id) ?>
            
        </div>

        <div class="d-flex flex-column pe-3 pb-3 w-100">
            


            <div class='d-flex align-items-center justify-content-between mt-3 pt-3 ms-3' style="border-top-width: 1px; border-top-style: solid;">
                Total
                
            </div>
        </div>
    </div>

    <div class="bg-white rounded-4 shadow px-4 py-4 mb-4" style="min-height: 550px; width: 40%">
        <div class="d-flex justify-content-between align-items-center px-3 mb-3">
            
            <button type="button" class="btn btn-primary d-flex align-items-center pe-3 border-0 rounded-5">
                <i class="bi bi-pencil d-flex align-items-center me-2"></i>Edit</button>
        </div>

        <img src="<?= esc($qr) ?>" alt="QR Code" />

        <button type="button" class="btn btn-success d-flex align-items-center pe-3 border-0 rounded-5">
                    <i class="bi bi-check d-flex align-items-center me-2"></i>Process</button>
    </div>


</div>

<?= $this->endSection() ?>