<?= $this->extend('desktop/template') ?>
<?= $this->section('content') ?>
<!-- Insert landing page content here -->
<div class="container d-flex w-100 px-5 pt-3">
    <div style='width: 55%'>
        <div class="bg-white rounded-5 me-4 shadow pb-4 pt-4">
            <div class="d-flex justify-content-between align-items-center px-5 mb-3">
                <h3 class="mb-0">Tables</h3>
            </div>
            <div class="d-flex rounded-5 mx-4 mt-4 px-2 py-4 w-auto flex-wrap justify-content-center" style="background-color: #f8f9fa;">
            <?php foreach ($tables as $table): ?>
                <a class="<?= $table['status'] == 'active' ? 'active' : '' ?> d-flex rounded-5 mx-2 my-2 py-4 border-0 justify-content-center align-items-center btn btn-light shadow"
                    style="width: 235px;" role="button" href='<?= base_url('tables/' . $table['table_id']); ?>'>
                    <div class="w-50" style="border-right-width: 2px; border-right-style: solid;">
                        <h5>T<?= sprintf('%02d', esc($table['table_id'])) ?></h5>
                        <?php if ($table['status'] == 'active'): ?>
                            <span class="badge text-bg-success">Active</span>
                        <?php else: ?>
                            <span class="badge text-bg-danger">Inactive</span>
                        <?php endif; ?>
                        <div class="icon-link icon-link-hover link-underline link-underline-opacity-0 ms-2 mt-2"
                            style="font-size: 14px">
                            Details
                            <i class="bi bi-arrow-right-short d-flex align-items-center"></i>
                        </div>
                    </div>
                    <div class="w-50">
                        <div class="text-start mb-0 d-block text-truncate align-top" style="height: 80px">
                            <?php foreach($tableOrders as $order): ?>

                                <?php if ($order['table_id'] == $table['table_id'] 
                                and $order['status'] != 'completed'
                                ): ?>
                                    <div class="ms-2 ps-1 me-2 mb-2" style="border-left-width: 1px; border-left-style: solid;">
                                        <?php foreach($orderItems as $item): ?>
                                            <?php if ($item['order_id'] == $order['order_id']): ?>
                                                <div class="d-flex justify-content-between" style="font-size: 10px;">
                                                    <p class="mb-0"><?= esc($item['name']) ?></p>
                                                    <p class='mb-0'>x <?= esc($item['amount']) ?></p>
                                                </div>
                                            <?php endif ?>
                                        <?php endforeach ?>
                                    </div>
                                <?php endif ?>
                            <?php endforeach ?>
                            <?php if($table['status'] != 'active'): ?>
                                    <p class="ms-2 ps-1 me-2 mb-2" style="font-size: 10px">No Orders</p>
                            <?php endif ?>
                        </div>
                    </div>
                </a>
            <?php endforeach ?>
        </div>
        </div>
    </div>
    <div style='width: 45%'>
        <div class="bg-white rounded-5 shadow pb-4 pt-4 px-4">
            <div class="d-flex justify-content-between align-items-center px-5 mb-3">
                <h3 class="mb-0">Orders</h3>
            </div>
            <div class="d-flex justify-content-center rounded-5 mt-4 p-4 w-auto flex-wrap" style="background-color: #f8f9fa; min-height: 150px;">

            <?php if (!$incoming): ?>
                <div class="w-100 d-flex align-items-center justify-content-center h-auto" style="color: #6c757d">
                <h5 class="mb-0">No Orders</h5>
                </div>
                
            <?php endif ?>
            <?php foreach ($incoming as $order): ?>
                <div class="d-flex bg-body flex-column rounded-4 mx-2 my-2 align-items-center overflow-hidden shadow h-100"
                    style="width: 300px;">
                    <div class='d-flex align-items-center justify-content-between w-100 text-bg-primary px-4 pt-2 pb-1'>
                        <h6 class='mb-0'>Order #<?= sprintf('%03d', esc($order['order_id'])) ?></h6>
                        <div class="d-flex mb-1">
                            <?php if ($order['status'] == 'incoming'): ?>
                                <p class='mb-0'><span class="badge text-bg-success ms-2">New</span></p>
                            <?php elseif ($order['status'] == 'processing'): ?>
                                <p class='mb-0'><span class="badge ms-2" style='background-color: #90B3C7'>Processing</span></p>
                            <?php elseif ($order['status'] == 'completed'): ?>
                                <p class='mb-0'><span class="badge ms-2" style='background-color: #6c757d'>Completed</span></p>
                            <?php endif; ?>
                            
                            <p class='mb-0'><span class="badge text-bg-secondary ms-2">T<?= sprintf('%02d', esc($order['table_id'])) ?></span></p>
                        </div>
                    </div>

                    <div class="d-flex flex-column pe-3 pb-3 w-100">
                        <ol class="mt-2 text-start mb-0">
                            <?php foreach($orderItems as $item): ?>
                                <?php if ($item['order_id'] == $order['order_id']): ?>
                                    <li class="d-flex justify-content-between">
                                        - <?= esc($item['name']) ?>
                                        <p class='mb-0 me-3'>x <?= esc($item['amount']) ?></p>
                                    </li>
                                <?php endif ?>
                            <?php endforeach ?>
                        </ol>


                        <div class='d-flex align-items-center justify-content-between mt-3 pt-3 ms-3' style="border-top-width: 1px; border-top-style: solid;">
                            <a class="icon-link icon-link-hover link-underline link-underline-opacity-0 ms-3 text-primary" href='<?= base_url('orders/' . $order['order_id']); ?>'>
                                <p class='mb-0 me-0'>Details</p>    
                                <i class="bi bi-arrow-right-short d-flex align-items-center ms-0"></i>
                            </a>
                            <button type="button" class="btn btn-success d-flex align-items-center pe-3 border-0 rounded-5">
                                <i class="bi bi-check d-flex align-items-center me-2"></i>Process</button>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
        </div>
    </div>

</div>

<?= $this->endSection() ?>