<?= $this->extend('desktop/admin/template') ?>
<?= $this->section('content') ?>
<!-- Insert landing page content here -->
<div class="container d-flex w-100 px-5 pt-3">
    <div class="bg-white rounded-5 py-4 shadow w-100">
        <div class="d-flex justify-content-between align-items-center px-5 mb-3">
            <h3 class="mb-0">All Restaurants</h3>
            <button type="button" class="btn btn-primary d-flex align-items-center pe-4 border-0">
                <i class="bi bi-plus fs-4 d-flex align-items-center"></i>Add Restaurant</button>
        </div>
        <table class="table table-lg">
            <thead>
                <tr>
                    <th class="ps-4" scope="col">ID</th>
                    <th style="min-width: 200px" scope="col">Name</th>
                    <th scope="col">Description</th>
                    <th scope="col">Tables</th>
                    <th class="pe-5" scope="col"></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($restaurants as $restaurant): ?>
                    <tr class="align-middle my-3">
                        <td class="text-center"><?= esc($restaurant['restaurant_id']) ?></td>
                        <td>
                            <div class="d-flex align-items-center">
                                <img src="<?= base_url($restaurant['image_path']); ?>"
                                    class="img-fluid me-3 object-fit-contain" style="width: 50px; height: 50px" alt="...">
                                <?= esc($restaurant['name']) ?>
                            </div>

                        </td>
                        <td><?= esc($restaurant['description']) ?></td>
                        <td class="text-center"><?= esc($restaurant['table_num']) ?></td>
                        <td class="dropdown">
                            <a href="" class="btn border-0" role="button" data-bs-toggle="dropdown">
                                <i class="bi bi-three-dots-vertical"></i>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a class="dropdown-item btn btn-light d-flex" href="#">
                                        <i class="bi bi-info d-flex align-items-center me-2"></i>Details
                                    </a>
                                </li>
                                <li>
                                    <a class="dropdown-item btn btn-light d-flex" href="#">
                                        <i class="bi bi-pencil d-flex align-items-center me-2"></i>Edit
                                    </a>
                                </li>
                                <li>
                                    <a class="dropdown-item btn btn-danger text-danger d-flex" href="#">
                                        <i class="bi bi-trash d-flex align-items-center me-2"></i>Delete
                                    </a>
                                </li>
                            </ul>
                        </td>
                    </tr>

                <?php endforeach ?>

            </tbody>
        </table>
    </div>

</div>

<?= $this->endSection() ?>