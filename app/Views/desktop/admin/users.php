<?= $this->extend('desktop/admin/template') ?>
<?= $this->section('content') ?>
<!-- Insert landing page content here -->
<div class="container d-flex flex-column w-100 px-5 pt-3">
    <div class="bg-white rounded-5 py-4 shadow w-100">
        <div class="d-flex justify-content-between px-5 mb-3">
            <h3 class="mb-0">All Users</h3>
            <button type="button" class="btn btn-primary d-flex align-items-center pe-4 border-0">
                <i class="bi bi-plus fs-4 d-flex align-items-center"></i>Add User</button>
        </div>
        <table class="table table-lg">
            <thead>
                <tr>
                    <th class="ps-4" scope="col">ID</th>
                    <th scope="col">Full Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Restaurant</th>
                    <th scope="col">Role</th>
                    <th class="pe-5" scope="col"></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($users as $user): ?>
                    <tr class="align-middle my-3">
                        <td class="text-center"><?= esc($user['user_id']) ?></td>
                        <td>
                            <div class="d-flex align-items-center">
                                <img src="<?= base_url($user['image_path']); ?>"
                                    class="img-fluid rounded-5 me-3 object-fit-cover" style="width: 50px; height: 50px"
                                    alt="...">
                                    <?= esc($user['first_name']) ?>     <?= esc($user['last_name']) ?>
                            </div>
                        </td>
                        <td><?= esc($user['email']) ?> </td>
                        <td>
                            <div class="d-flex align-items-center">
                                <?php foreach ($restaurants as $restaurant): ?>
                                    <?php if ($restaurant['restaurant_id'] == $user['restaurant_id']): ?>
                                        <img src="<?= base_url($restaurant['image_path']); ?>"
                                            class="img-fluid me-3 object-fit-contain" style="width: 50px; height: 50px" alt="...">
                                    <?php endif; ?>
                                    <?= $restaurant['restaurant_id'] == $user['restaurant_id'] ? $restaurant['name'] : '' ?>
                                <?php endforeach ?>
                            </div>
                        </td>
                        <td><?= esc($user['name']) ?></td>
                        <td class="dropdown">
                            <a href="" class="btn border-0" role="button" data-bs-toggle="dropdown">
                                <i class="bi bi-three-dots-vertical"></i>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item btn btn-light d-flex" href="#">
                                        <i class="bi bi-info d-flex align-items-center me-2"></i>Details</a>
                                </li>

                                <?php if ($user['role_id'] != 1): ?>
                                    <li><a class="dropdown-item btn btn-light d-flex" href="#">
                                            <i class="bi bi-pencil d-flex align-items-center me-2"></i>Edit</a>
                                    </li>
                                    <li><a class="dropdown-item btn btn-danger text-danger d-flex" href="#">
                                            <i class="bi bi-trash d-flex align-items-center me-2"></i>Delete</a>
                                    </li>
                                <?php endif; ?>

                            </ul>
                        </td>
                    </tr>

                <?php endforeach ?>

            </tbody>
        </table>
    </div>

    <div class="bg-white rounded-5 py-4 shadow w-100 my-5">
        <h3 class="px-5 mb-3">Managers</h3>
        <table class="table table-lg">
            <thead>
                <tr>
                    <th class="ps-4" scope="col">ID</th>
                    <th scope="col">Full Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Restaurant</th>
                    <th class="pe-5" scope="col"></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($userManagers as $userManager): ?>
                    <tr class="align-middle my-3">
                        <td class="text-center"><?= esc($userManager['user_id']) ?></td>
                        <td>
                            <div class="d-flex align-items-center">
                                <img src="<?= base_url($userManager['image_path']); ?>"
                                    class="img-fluid rounded-5 me-3 object-fit-cover" style="width: 50px; height: 50px"
                                    alt="...">
                                    <?= esc($userManager['first_name']) ?>     <?= esc($userManager['last_name']) ?>
                            </div>
                        </td>
                        <td><?= esc($userManager['email']) ?> </td>
                        <td>
                            <div class="d-flex align-items-center">
                                <?php foreach ($restaurants as $restaurant): ?>
                                    <?php if ($restaurant['restaurant_id'] == $userManager['restaurant_id']): ?>
                                        <img src="<?= base_url($restaurant['image_path']); ?>"
                                            class="img-fluid me-3 object-fit-contain" style="width: 50px; height: 50px" alt="...">
                                    <?php endif; ?>
                                    <?= $restaurant['restaurant_id'] == $userManager['restaurant_id'] ? $restaurant['name'] : '' ?>
                                <?php endforeach ?>
                            </div>
                        </td>                        
                        <td class="dropdown">
                            <a href="" class="btn border-0" role="button" data-bs-toggle="dropdown">
                                <i class="bi bi-three-dots-vertical"></i>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item btn btn-light d-flex" href="#">
                                        <i class="bi bi-info d-flex align-items-center me-2"></i>Details</a>
                                </li>

                                <?php if ($user['role_id'] != 1): ?>
                                    <li><a class="dropdown-item btn btn-light d-flex" href="#">
                                            <i class="bi bi-pencil d-flex align-items-center me-2"></i>Edit</a>
                                    </li>
                                    <li><a class="dropdown-item btn btn-danger text-danger d-flex" href="#">
                                            <i class="bi bi-trash d-flex align-items-center me-2"></i>Delete</a>
                                    </li>
                                <?php endif; ?>

                            </ul>
                        </td>
                    </tr>

                <?php endforeach ?>

            </tbody>
        </table>
    </div>

    <div class="bg-white rounded-5 py-4 shadow w-100 mb-5">
        <h3 class="px-5 mb-3">Staff</h3>
        <table class="table table-lg">
            <thead>
                <tr>
                    <th class="ps-4" scope="col">ID</th>
                    <th scope="col">Full Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Restaurant</th>
                    <th class="pe-5" scope="col"></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($userStaff as $user): ?>
                    <tr class="align-middle my-3">
                        <td class="text-center"><?= esc($user['user_id']) ?></td>
                        <td>
                            <div class="d-flex align-items-center">
                                <img src="<?= base_url($user['image_path']); ?>"
                                    class="img-fluid rounded-5 me-3 object-fit-cover" style="width: 50px; height: 50px"
                                    alt="...">
                                    <?= esc($user['first_name']) ?>     <?= esc($user['last_name']) ?>
                            </div>
                        </td>
                        <td><?= esc($user['email']) ?> </td>
                        <td>
                            <div class="d-flex align-items-center">
                                <?php foreach ($restaurants as $restaurant): ?>
                                    <?php if ($restaurant['restaurant_id'] == $user['restaurant_id']): ?>
                                        <img src="<?= base_url($restaurant['image_path']); ?>"
                                            class="img-fluid me-3 object-fit-contain" style="width: 50px; height: 50px" alt="...">
                                    <?php endif; ?>
                                    <?= $restaurant['restaurant_id'] == $user['restaurant_id'] ? $restaurant['name'] : '' ?>
                                <?php endforeach ?>
                            </div>

                        </td>
                        <td class="dropdown">
                            <a href="" class="btn border-0" role="button" data-bs-toggle="dropdown">
                                <i class="bi bi-three-dots-vertical"></i>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item btn btn-light d-flex" href="#">
                                        <i class="bi bi-info d-flex align-items-center me-2"></i>Details</a>
                                </li>

                                <?php if ($user['role_id'] != 1): ?>
                                    <li><a class="dropdown-item btn btn-light d-flex" href="#">
                                            <i class="bi bi-pencil d-flex align-items-center me-2"></i>Edit</a>
                                    </li>
                                    <li><a class="dropdown-item btn btn-danger text-danger d-flex" href="#">
                                            <i class="bi bi-trash d-flex align-items-center me-2"></i>Delete</a>
                                    </li>
                                <?php endif; ?>

                            </ul>
                        </td>
                    </tr>

                <?php endforeach ?>

            </tbody>
        </table>
    </div>

</div>

<?= $this->endSection() ?>