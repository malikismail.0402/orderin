<?= $this->extend('desktop/template') ?>
<?= $this->section('content') ?>
<!-- Insert landing page content here -->
<div class="container d-flex w-100 px-5 pt-3 flex-column">
    <div class="bg-white rounded-5 shadow w-100 px-4 py-4 mb-4">
        <div class="d-flex justify-content-between align-items-center px-3 mb-3">
            <h3 class="mb-0">Incoming Orders</h3>
            <button type="button" class="btn btn-primary d-flex align-items-center pe-3 border-0 rounded-5">
                <i class="bi bi-plus d-flex align-items-center me-2"></i>New Order</button>
        </div>
        <div class="d-flex rounded-5 mt-4 p-4 w-auto flex-wrap" style="background-color: #f8f9fa; min-height: 150px;">

            <?php if (!$incoming): ?>
                <div class="w-100 d-flex align-items-center justify-content-center h-auto" style="color: #6c757d">
                <h5 class="mb-0">No Orders</h5>
                </div>
                
            <?php endif ?>
            <?php foreach ($incoming as $order): ?>
                <div class="d-flex bg-body flex-column rounded-4 mx-2 my-2 align-items-center overflow-hidden shadow h-100"
                    style="width: 300px;">
                    <div class='d-flex align-items-center justify-content-between w-100 text-bg-primary px-4 pt-2 pb-1'>
                        <h6 class='mb-0'>Order #<?= sprintf('%03d', esc($order['order_id'])) ?></h6>
                        <div class="d-flex mb-1">
                            <?php if ($order['status'] == 'incoming'): ?>
                                <p class='mb-0'><span class="badge text-bg-success ms-2">New</span></p>
                            <?php elseif ($order['status'] == 'processing'): ?>
                                <p class='mb-0'><span class="badge ms-2" style='background-color: #90B3C7'>Processing</span></p>
                            <?php elseif ($order['status'] == 'completed'): ?>
                                <p class='mb-0'><span class="badge ms-2" style='background-color: #6c757d'>Completed</span></p>
                            <?php endif; ?>
                            
                            <p class='mb-0'><span class="badge text-bg-secondary ms-2">T<?= sprintf('%02d', esc($order['table_id'])) ?></span></p>
                        </div>
                    </div>

                    <div class="d-flex flex-column pe-3 pb-3 w-100">
                        <ol class="mt-2 text-start mb-0">
                            <?php foreach($orderItems as $item): ?>
                                <?php if ($item['order_id'] == $order['order_id']): ?>
                                    <li class="d-flex justify-content-between">
                                        - <?= esc($item['name']) ?>
                                        <p class='mb-0 me-3'>x <?= esc($item['amount']) ?></p>
                                    </li>
                                <?php endif ?>
                            <?php endforeach ?>
                        </ol>


                        <div class='d-flex align-items-center justify-content-between mt-3 pt-3 ms-3' style="border-top-width: 1px; border-top-style: solid;">
                            <a class="icon-link icon-link-hover link-underline link-underline-opacity-0 ms-3 text-primary" href='<?= base_url('orders/' . $order['order_id']); ?>'>
                                <p class='mb-0 me-0'>Details</p>    
                                <i class="bi bi-arrow-right-short d-flex align-items-center ms-0"></i>
                            </a>
                            <a type="button" href="" class="btn btn-success d-flex align-items-center pe-3 border-0 rounded-5">
                                <i class="bi bi-check d-flex align-items-center me-2"></i>Process</a>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
        </div>

    </div>

    <div class="bg-white rounded-5 shadow w-100 px-4 py-4 mb-4">
        <div class="d-flex justify-content-between align-items-center px-3 mb-3">
            <h3 class="mb-0">Processing</h3>
        </div>
        <div class="d-flex rounded-5 mt-4 p-4 w-auto flex-wrap" style="background-color: #f8f9fa; min-height: 150px;">
            <?php if (!$processing): ?>
                <div class="w-100 d-flex align-items-center justify-content-center h-auto" style="color: #6c757d">
                <h5 class="mb-0">No Orders</h5>
                </div>
                
            <?php endif ?>
            <?php foreach ($processing as $order): ?>
                <div class="d-flex bg-body flex-column rounded-4 mx-2 my-2 align-items-center overflow-hidden shadow h-100"
                    style="width: 300px;">
                    <div class='d-flex align-items-center justify-content-between w-100 text-bg-primary px-4 pt-2 pb-1'>
                        <h6 class='mb-0'>Order #<?= sprintf('%03d', esc($order['order_id'])) ?></h6>
                        <div class="d-flex mb-1">
                            <?php if ($order['status'] == 'incoming'): ?>
                                <p class='mb-0'><span class="badge text-bg-success ms-2">New</span></p>
                            <?php elseif ($order['status'] == 'processing'): ?>
                                <p class='mb-0'><span class="badge ms-2" style='background-color: #90B3C7'>Processing</span></p>
                            <?php elseif ($order['status'] == 'completed'): ?>
                                <p class='mb-0'><span class="badge ms-2" style='background-color: #6c757d'>Completed</span></p>
                            <?php endif; ?>
                            
                            <p class='mb-0'><span class="badge text-bg-secondary ms-2">T<?= sprintf('%02d', esc($order['table_id'])) ?></span></p>
                        </div>
                    </div>

                    <div class="d-flex flex-column pe-3 pb-3 w-100">
                    <ol class="mt-2 text-start mb-0">
                            <?php foreach($orderItems as $item): ?>
                                <?php if ($item['order_id'] == $order['order_id']): ?>
                                    <li class="d-flex justify-content-between">
                                        - <?= esc($item['name']) ?>
                                        <p class='mb-0 me-3'>x <?= esc($item['amount']) ?></p>
                                    </li>
                                <?php endif ?>
                            <?php endforeach ?>
                        </ol>


                        <div class='d-flex align-items-center justify-content-between mt-3 pt-3 ms-3' style="border-top-width: 1px; border-top-style: solid;">
                            <a class="icon-link icon-link-hover link-underline link-underline-opacity-0 ms-3 text-primary" href='<?= base_url('orders/' . $order['order_id']); ?>'>
                                <p class='mb-0 me-0'>Details</p>    
                                <i class="bi bi-arrow-right-short d-flex align-items-center ms-0"></i>
                            </a>
                            <button type="button" class="btn btn-secondary d-flex align-items-center pe-3 border-0 rounded-5">
                                <i class="bi bi-check d-flex align-items-center me-2"></i>Complete</button>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
        </div>

    </div>

    <div class="bg-white rounded-5 shadow w-100 px-4 py-4 mb-4">
        <div class="d-flex justify-content-between align-items-center px-3 mb-3">
            <h3 class="mb-0">Completed Orders</h3>
        </div>
        <div class="d-flex rounded-5 mt-4 p-4 w-auto flex-wrap" style="background-color: #f8f9fa; min-height: 150px;">
            <?php if (!$completed): ?>
                <div class="w-100 d-flex align-items-center justify-content-center h-auto" style="color: #6c757d">
                <h5 class="mb-0">No Orders</h5>
                </div>
                
            <?php endif ?>
            <?php foreach ($completed as $order): ?>
                <div class="d-flex bg-body flex-column rounded-4 mx-2 my-2 align-items-center overflow-hidden shadow h-100"
                    style="width: 300px;">
                    <div class='d-flex align-items-center justify-content-between w-100 text-bg-primary px-4 pt-2 pb-1'>
                        <h6 class='mb-0'>Order #<?= sprintf('%03d', esc($order['order_id'])) ?></h6>
                        <div class="d-flex mb-1">
                            <?php if ($order['status'] == 'incoming'): ?>
                                <p class='mb-0'><span class="badge text-bg-success ms-2">New</span></p>
                            <?php elseif ($order['status'] == 'processing'): ?>
                                <p class='mb-0'><span class="badge ms-2" style='background-color: #90B3C7'>Processing</span></p>
                            <?php elseif ($order['status'] == 'completed'): ?>
                                <p class='mb-0'><span class="badge ms-2" style='background-color: #6c757d'>Completed</span></p>
                            <?php endif; ?>
                            
                            <p class='mb-0'><span class="badge text-bg-secondary ms-2">T<?= sprintf('%02d', esc($order['table_id'])) ?></span></p>
                        </div>
                    </div>

                    <div class="d-flex flex-column pe-3 pb-3 w-100">
                        <ol class="mt-2 text-start mb-0">
                            <?php foreach($orderItems as $item): ?>
                                <?php if ($item['order_id'] == $order['order_id']): ?>
                                    <li class="d-flex justify-content-between">
                                        - <?= esc($item['name']) ?>
                                        <p class='mb-0 me-3'>x <?= esc($item['amount']) ?></p>
                                    </li>
                                <?php endif ?>
                            <?php endforeach ?>
                        </ol>


                        <div class='d-flex align-items-center justify-content-between mt-3 pt-3 ms-3' style="border-top-width: 1px; border-top-style: solid;">
                            <a class="icon-link icon-link-hover link-underline link-underline-opacity-0 ms-3 text-primary" href='<?= base_url('orders/' . $order['order_id']); ?>'>
                                <p class='mb-0 me-0'>Details</p>    
                                <i class="bi bi-arrow-right-short d-flex align-items-center ms-0"></i>
                            </a>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
        </div>

    </div>

    <div class="bg-white rounded-5 shadow w-100 px-4 py-4 mb-5">
        <div class="d-flex justify-content-between align-items-center px-3 mb-3">
            <h3 class="mb-0">Order History</h3>
        </div>
        <div class="d-flex rounded-5 mt-4 p-4 w-auto flex-wrap" style="background-color: #f8f9fa; min-height: 150px;">
            <?php if (!$orders): ?>
                <div class="w-100 d-flex align-items-center justify-content-center h-auto" style="color: #6c757d">
                <h5 class="mb-0">No Orders</h5>
                </div>
            <?php endif ?>
            <?php foreach ($orders as $order): ?>
                <div class="d-flex bg-body flex-column rounded-4 mx-2 my-2 align-items-center overflow-hidden shadow h-100"
                    style="width: 300px;">
                    <div class='d-flex align-items-center justify-content-between w-100 text-bg-primary px-4 pt-2 pb-1'>
                        <h6 class='mb-0'>Order #<?= sprintf('%03d', esc($order['order_id'])) ?></h6>
                        <div class="d-flex mb-1">
                            <?php if ($order['status'] == 'incoming'): ?>
                                <p class='mb-0'><span class="badge text-bg-success ms-2">New</span></p>
                            <?php elseif ($order['status'] == 'processing'): ?>
                                <p class='mb-0'><span class="badge ms-2" style='background-color: #90B3C7'>Processing</span></p>
                            <?php elseif ($order['status'] == 'completed'): ?>
                                <p class='mb-0'><span class="badge ms-2" style='background-color: #6c757d'>Completed</span></p>
                            <?php endif; ?>
                            
                            <p class='mb-0'><span class="badge text-bg-secondary ms-2">T<?= sprintf('%02d', esc($order['table_id'])) ?></span></p>
                        </div>
                    </div>

                    <div class="d-flex flex-column pe-3 pb-3 w-100">
                        <ol class="mt-2 text-start mb-0">
                            <?php foreach($orderItems as $item): ?>
                                <?php if ($item['order_id'] == $order['order_id']): ?>
                                    <li class="d-flex justify-content-between">
                                        - <?= esc($item['name']) ?>
                                        <p class='mb-0 me-3'>x <?= esc($item['amount']) ?></p>
                                    </li>
                                <?php endif ?>
                            <?php endforeach ?>
                        </ol>


                        <div class='d-flex align-items-center justify-content-between mt-3 pt-3 ms-3' style="border-top-width: 1px; border-top-style: solid;">
                            <a class="icon-link icon-link-hover link-underline link-underline-opacity-0 ms-3 text-primary" href='<?= base_url('orders/' . $order['order_id']); ?>'>
                                <p class='mb-0 me-0'>Details</p>    
                                <i class="bi bi-arrow-right-short d-flex align-items-center ms-0"></i>
                            </a>
                        </div>
                    </div>

                    
                    

                </div>
            <?php endforeach ?>
        </div>

    </div>

</div>

<?= $this->endSection() ?>