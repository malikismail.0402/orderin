<?= $this->extend('desktop/template') ?>
<?= $this->section('content') ?>
<!-- Insert landing page content here -->
<div class="container d-flex w-100 px-5 pt-3 justify-content-center">
    <div class="d-flex bg-body flex-column rounded-4 align-items-center overflow-hidden shadow h-100 me-4"
        style="min-height: 550px; width: 50%">
        <div class='d-flex align-items-center justify-content-between w-100 text-bg-primary px-4 pt-2 pb-1'>
            <h3 class='mb-0'>Items</h3>
            
        </div>

        <div class="d-flex flex-column pe-3 pb-3 w-100">
            <ol class="mt-2 text-start mb-0">
                <?php foreach($orderItems as $item): ?>
                    <?php if ($item['order_id'] == $order['order_id']): ?>
                        <li class="d-flex justify-content-between">
                            - <?= esc($item['name']) ?>
                            <p class='mb-0 me-3'>x <?= esc($item['amount']) ?></p>
                        </li>
                    <?php endif ?>
                <?php endforeach ?>
            </ol>


            <div class='d-flex align-items-center justify-content-between mt-3 pt-3 ms-3' style="border-top-width: 1px; border-top-style: solid;">
                Total
                
            </div>
        </div>
    </div>

    <div class="bg-white rounded-4 shadow px-4 py-4 mb-4" style="min-height: 550px; width: 40%">
        <div class="d-flex justify-content-between align-items-center px-3 mb-3">
            <div class="d-flex mb-1 align-items-center flex-column">
                <h3 class="mb-0 mt-2">Order #<?= sprintf('%03d', esc($id)) ?></h3>
                <div class="d-flex">
                    <?php if ($order['status'] == 'incoming'): ?>
                        <p class='mb-0 fs-4'><span class="badge text-bg-success ms-2">New</span></p>
                    <?php elseif ($order['status'] == 'processing'): ?>
                        <p class='mb-0 fs-4'><span class="badge ms-2" style='background-color: #90B3C7'>Processing</span></p>
                    <?php elseif ($order['status'] == 'completed'): ?>
                        <p class='mb-0 fs-4'><span class="badge ms-2" style='background-color: #6c757d'>Completed</span></p>
                    <?php endif; ?>
                    
                    <p class='mb-0 fs-4'><span class="badge text-bg-secondary ms-2">Table <?= sprintf('%02d', esc($order['table_id'])) ?></span></p>
                </div>
            </div>
            <button type="button" class="btn btn-primary d-flex align-items-center pe-3 border-0 rounded-5">
                <i class="bi bi-pencil d-flex align-items-center me-2"></i>Edit</button>
        </div>
        <button type="button" class="btn btn-success d-flex align-items-center pe-3 border-0 rounded-5">
                    <i class="bi bi-check d-flex align-items-center me-2"></i>Process</button>
    </div>


</div>

<?= $this->endSection() ?>