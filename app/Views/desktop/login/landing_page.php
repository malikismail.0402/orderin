<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>orderIN</title>
    <!-- This is the main stylesheet for Bootstrap. It includes all the CSS necessary for Bootstrap's components and utilities to work. -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <!-- Include Bootstrap Icons -->
    <!-- This link imports the Bootstrap Icons library, which provides a wide range of SVG icons for use in your projects. -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">
  </head>
  <body>
    <header>
      <nav class="navbar navbar-expand-lg navbar-dark bg-blue-100">
        <div class="container">
            <div class="pe-1" style=" width: 175px; height: 50px">
                <img src="<?= base_url('orderIN.png'); ?>" class="img-fluid" alt="...">
        </div>
              
              <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
              </button>
              <div class="collapse navbar-collapse" id="navbarNav">
                  <ul class="navbar-nav ms-auto">
                      
                      <li class="nav-item">
                          <a class="nav-link text-black" href="<?= base_url('login'); ?>">Login</a>
                      </li>
                  </ul>
              </div>
          </div>
      </nav>
  </header>

  <main>
      <section class="py-5 bg-light">
          <div class="container">
              <div class="row">
                  <div class="col-lg-6">
                      <h1 class="display-4">Get help taking orders in your Restaurant!</h1>
                      <p class="lead">orderIN simplifies the process of managing your restaurant for increased efficiency.</p>
                      <a href="<?= base_url('login'); ?>" class="btn btn-primary btn-lg mb-3 mb-lg-0">Get Started</a>
                  </div>
                  <div class="col-lg-6">
                  </div>
              </div>
          </div>
      </section>

      <section class="py-5">
          <div class="container">
              <h2 class="text-center mb-4">Key Features</h2>
              <div class="row">
                  <div class="col-lg-4 mb-4">
                      <div class="card">
                          <div class="card-body">
                              <h4 class="card-title">Manage Tables and Orders</h4>
                              <p class="card-text">Manage Your Tables and Orders</p>
                          </div>
                      </div>
                  </div>
                  <div class="col-lg-4 mb-4">
                      <div class="card">
                          <div class="card-body">
                              <h4 class="card-title">Manage Restaurant and Staff</h4>
                              <p class="card-text">Manage Your Restaurant and Staff.</p>
                          </div>
                      </div>
                  </div>
                  <div class="col-lg-4 mb-4">
                      <div class="card">
                          <div class="card-body">
                              <h4 class="card-title">QR Code</h4>
                              <p class="card-text">Scan QR Code to access the Menu</p>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </section>
  </main>

  <footer class="bg-dark text-light py-4">
      <div class="container">
          <div class="row">
              <div class="col-md-6">
                  <p>&copy; 2024 orderIN. All rights reserved.</p>
              </div>
              <div class="col-md-6 text-md-end">
                  <a href="#" class="text-light me-3">Privacy Policy</a>
                  <a href="#" class="text-light">Terms of Service</a>
              </div>
          </div>
      </div>
  </footer>
    <!-- This script includes all of Bootstrap's JavaScript-based components and behaviors, such as modal windows, dropdowns, and tooltips.  -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
  </body>
</html>