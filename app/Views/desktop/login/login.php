<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>orderIN - Login</title>
  <!-- This is the main stylesheet for Bootstrap. It includes all the CSS necessary for Bootstrap's components and utilities to work. -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
  <!-- Include Bootstrap Icons -->
  <!-- This link imports the Bootstrap Icons library, which provides a wide range of SVG icons for use in your projects. -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Jost:ital,wght@0,100..900;1,100..900&display=swap"
    rel="stylesheet">
</head>

<body class="container-fluid d-flex vh-100 p-0 justify-content-center" style="
  background-color: #f8f9fa; font-family: 'Jost'; sans-serif !important; 
  ">
  <style>
    .btn-light {
      --bs-btn-bg: white;
      --bs-btn-hover-bg: #E2C691;
      --bs-btn-hover-color: #1E5A7C;
      --bs-btn-active-bg: #E2C691;
      --bs-btn-active-color: #1E5A7C;
      --bs-btn-active-border-color: #1E5A7C;
      --bs-btn-hover-border-color: #1E5A7C;
      border-color: #1E5A7C;
    }

    .btn-primary {
      --bs-btn-bg: #1E5A7C;
      --bs-btn-hover-bg: #E2C691;
      --bs-btn-hover-color: #1E5A7C;
      --bs-btn-active-bg: #E2C691;
      --bs-btn-active-color: #1E5A7C;
      --bs-btn-active-border-color: #E2C691;
      --bs-btn-hover-border-color: #1E5A7C;
      border-color: white;
    }

    .btn-primary-hollow {
      color: #1E5A7C;
      --bs-btn-bg: white;
      --bs-btn-hover-bg: #1E5A7C;
      --bs-btn-hover-color: white;
      --bs-btn-active-bg: #1E5A7C;
      --bs-btn-active-color: white;
      --bs-btn-active-border-color: #E2C691;
      --bs-btn-hover-border-color: #1E5A7C;
      border-color: #1E5A7C;
    }

    .btn-secondary {
      color: #1E5A7C;
      --bs-btn-bg: #E2C691;
      --bs-btn-hover-bg: #1E5A7C;
      --bs-btn-hover-color: white;
      --bs-btn-active-bg: #1E5A7C;
      --bs-btn-active-color: white;
      --bs-btn-active-border-color: #1E5A7C;
      --bs-btn-hover-border-color: #E2C691;
      border-color: white;
    }

    .btn-danger {
      color: #dc3545;
      --bs-btn-bg: white;
      --bs-btn-hover-color: white;
      --bs-btn-active-color: white;
      border-color: white;
    }

    .gsi-material-button {
        -moz-user-select: none;
        -webkit-user-select: none;
        -ms-user-select: none;
        -webkit-appearance: none;
        background-color: WHITE;
        background-image: none;
        border: 1px solid #747775;
        -webkit-border-radius: 20px;
        border-radius: 20px;
        -webkit-box-sizing: border-box;
        box-sizing: border-box;
        color: #1f1f1f;
        cursor: pointer;
        font-family: 'Roboto', arial, sans-serif;
        font-size: 14px;
        height: 40px;
        letter-spacing: 0.25px;
        outline: none;
        overflow: hidden;
        padding: 0 12px;
        position: relative;
        text-align: center;
        -webkit-transition: background-color .218s, border-color .218s, box-shadow .218s;
        transition: background-color .218s, border-color .218s, box-shadow .218s;
        vertical-align: middle;
        white-space: nowrap;
        width: auto;
        max-width: 400px;
        min-width: min-content;
        }

        .gsi-material-button .gsi-material-button-icon {
        height: 20px;
        margin-right: 12px;
        min-width: 20px;
        width: 20px;
        }

        .gsi-material-button .gsi-material-button-content-wrapper {
        -webkit-align-items: center;
        align-items: center;
        display: flex;
        -webkit-flex-direction: row;
        flex-direction: row;
        -webkit-flex-wrap: nowrap;
        flex-wrap: nowrap;
        height: 100%;
        justify-content: space-between;
        position: relative;
        width: 100%;
        }

        .gsi-material-button .gsi-material-button-contents {
        -webkit-flex-grow: 1;
        flex-grow: 1;
        font-family: 'Roboto', arial, sans-serif;
        font-weight: 500;
        overflow: hidden;
        text-overflow: ellipsis;
        vertical-align: top;
        }

        .gsi-material-button .gsi-material-button-state {
        -webkit-transition: opacity .218s;
        transition: opacity .218s;
        bottom: 0;
        left: 0;
        opacity: 0;
        position: absolute;
        right: 0;
        top: 0;
        }

        .gsi-material-button:disabled {
        cursor: default;
        background-color: #ffffff61;
        border-color: #1f1f1f1f;
        }

        .gsi-material-button:disabled .gsi-material-button-contents {
        opacity: 38%;
        }

        .gsi-material-button:disabled .gsi-material-button-icon {
        opacity: 38%;
        }

        .gsi-material-button:not(:disabled):active .gsi-material-button-state, 
        .gsi-material-button:not(:disabled):focus .gsi-material-button-state {
        background-color: #303030;
        opacity: 12%;
        }

        .gsi-material-button:not(:disabled):hover {
        -webkit-box-shadow: 0 1px 2px 0 rgba(60, 64, 67, .30), 0 1px 3px 1px rgba(60, 64, 67, .15);
        box-shadow: 0 1px 2px 0 rgba(60, 64, 67, .30), 0 1px 3px 1px rgba(60, 64, 67, .15);
        }

        .gsi-material-button:not(:disabled):hover .gsi-material-button-state {
        background-color: #303030;
        opacity: 8%;
        }


  </style>

  <div class="bg-body shadow fixed-top">
    <div class="d-flex justify-content-between align-items-center mb-3 pt-2 mt-0" style="height: 80px">
      <a class="btn border-0 d-flex mt-2 ms-3" href="<?= base_url(''); ?>">
        <i class="bi bi-arrow-left fs-3 d-flex align-items-center me-2"></i>
      </a>
      <div class="pe-1 me-5" style=" width: 175px; height: 50px">
        <img src="<?= base_url('orderIN.png'); ?>" class="img-fluid" alt="...">
      </div>
      <div class="mx-3"></div>
    </div>

  </div>

  <div class="d-flex flex-column" style="padding-top: 100px; width: 900px">

    <div class="bg-white rounded-5 my-5 py-4 d-flex justify-content-center shadow  ">
        <div class='my-4' style="width: 350px">
            <div class='text-center'>
                <h3 class='fw-semibold'>Login</h3>
                <p class='fw-light' style="color: #6c757d">Welcome back!</p>
            </div>
            
            <form method="POST" action="<?= base_url('login'); ?>">
                <div class="mb-3">
                    <label for="email" class="form-label">Email</label>
                    <input type="text" class="form-control rounded-5" id="email" name="email" required>
                </div>
                <div class="mb-3">
                    <label for="password" class="form-label">Password</label>
                    <input type="password" class="form-control rounded-5" id="password" name="password" required>
                </div>
                <?php if (session()->getFlashdata('success')): ?>
                    <div class="alert alert-success">
                        <?= session()->getFlashdata('success'); ?>
                    </div>
                <?php endif; ?>

                <?php if (session()->getFlashdata('error')): ?>
                    <div class="alert alert-danger">
                        <?= session()->getFlashdata('error'); ?>
                    </div>
                <?php endif; ?>
                <button type="submit" class="btn btn-primary w-100 border-0 mt-3 rounded-5">Login</button>
            </form>
            <a href="<?= base_url('register'); ?>" class="btn btn-primary-hollow w-100 mt-3 rounded-5" role="button">Register as Staff</a>

            <p class='fw-light text-secondary-emphasis w-100 text-center border-bottom mt-4 px-2' style="line-height: 0.1em"><span class="bg-white px-2">or</span></p>

            <a href="<?= base_url('login/google'); ?>">
            <button class="gsi-material-button mt-3 mb-4" style="width:350px">
            <div class="gsi-material-button-state"></div>
            <div class="gsi-material-button-content-wrapper">
                <div class="gsi-material-button-icon" style="margin-left: 85px">
                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 48" xmlns:xlink="http://www.w3.org/1999/xlink" style="display: block;">
                    <path fill="#EA4335" d="M24 9.5c3.54 0 6.71 1.22 9.21 3.6l6.85-6.85C35.9 2.38 30.47 0 24 0 14.62 0 6.51 5.38 2.56 13.22l7.98 6.19C12.43 13.72 17.74 9.5 24 9.5z"></path>
                    <path fill="#4285F4" d="M46.98 24.55c0-1.57-.15-3.09-.38-4.55H24v9.02h12.94c-.58 2.96-2.26 5.48-4.78 7.18l7.73 6c4.51-4.18 7.09-10.36 7.09-17.65z"></path>
                    <path fill="#FBBC05" d="M10.53 28.59c-.48-1.45-.76-2.99-.76-4.59s.27-3.14.76-4.59l-7.98-6.19C.92 16.46 0 20.12 0 24c0 3.88.92 7.54 2.56 10.78l7.97-6.19z"></path>
                    <path fill="#34A853" d="M24 48c6.48 0 11.93-2.13 15.89-5.81l-7.73-6c-2.15 1.45-4.92 2.3-8.16 2.3-6.26 0-11.57-4.22-13.47-9.91l-7.98 6.19C6.51 42.62 14.62 48 24 48z"></path>
                    <path fill="none" d="M0 0h48v48H0z"></path>
                </svg>
                </div>
                <span class="gsi-material-button-contents text-start">Sign in with Google</span>
                <span style="display: none;">Sign in with Google</span>
            </div>
            </button>
        </a>

        <div class="text-center">
        <a href="<?= base_url('register-restaurant'); ?>">
        <div class="icon-link icon-link-hover link-underline link-underline-opacity-0 ms-2 mt-2" style="color: #1E5A7C">
                        Register your own Restaurant
                        <i class="bi bi-arrow-right-short d-flex align-items-center"></i>
                    </div>
        </a>
        
        </div>
        
            
        </div>

        
    </div>

  </div>







  <!-- This script includes all of Bootstrap's JavaScript-based components and behaviors, such as modal windows, dropdowns, and tooltips.  -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz"
    crossorigin="anonymous"></script>
</body>

</html>