<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>orderIN - <?= esc($title) ?></title>
  <!-- This is the main stylesheet for Bootstrap. It includes all the CSS necessary for Bootstrap's components and utilities to work. -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
  <!-- Include Bootstrap Icons -->
  <!-- This link imports the Bootstrap Icons library, which provides a wide range of SVG icons for use in your projects. -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Jost:ital,wght@0,100..900;1,100..900&display=swap"
    rel="stylesheet">
</head>

<body class="container-fluid d-flex vh-100 p-0" style="
  background-color: #f8f9fa; font-family: 'Jost'; sans-serif !important; 
  ">
  <style>
    .btn-light {
      --bs-btn-bg: white;
      --bs-btn-hover-bg: #E2C691;
      --bs-btn-hover-color: #1E5A7C;
      --bs-btn-active-bg: #E2C691;
      --bs-btn-active-color: #1E5A7C;
      --bs-btn-active-border-color: #1E5A7C;
      --bs-btn-hover-border-color: #1E5A7C;
      border-color: white;
    }

    .btn-primary {
      --bs-btn-bg: #1E5A7C;
      --bs-btn-hover-bg: #E2C691;
      --bs-btn-hover-color: #1E5A7C;
      --bs-btn-active-bg: #E2C691;
      --bs-btn-active-color: #1E5A7C;
      --bs-btn-active-border-color: #E2C691;
      --bs-btn-hover-border-color: #1E5A7C;
      border-color: white;
    }

    .btn-secondary {
      color: #1E5A7C;
      --bs-btn-bg: #E2C691;
      --bs-btn-hover-bg: #1E5A7C;
      --bs-btn-hover-color: white;
      --bs-btn-active-bg: #1E5A7C;
      --bs-btn-active-color: white;
      --bs-btn-active-border-color: #1E5A7C;
      --bs-btn-hover-border-color: #E2C691;
      border-color: white;
    }

    .btn-danger {
      color: #dc3545;
      --bs-btn-bg: white;
      --bs-btn-hover-color: white;
      --bs-btn-active-color: white;
      border-color: white;
    }

    .text-bg-primary {
      background-color: #1E5A7C !important;
      color: #E2C691 !important;
    }

    .text-bg-secondary {
      background-color: #E2C691 !important;
      color: #1E5A7C !important;
    }

    .text-primary {
      color: #1E5A7C !important;
    }
  </style>

  <div class="bg-body h-100 shadow fixed-top" style="width: 250px;">
    <div class="d-flex justify-content-center align-items-center" style="height: 100px">
      <a class="pe-1" style=" width: 175px; height: 50px" href="<?= base_url('dashboard'); ?>">
        <img src="<?= base_url('orderIN.png'); ?>" class="img-fluid" alt="...">
      </a>
    </div>


    <div class="d-flex flex-column pb-4">
      <p class="px-5 pt-3 mb-2 fw-light">Features</p>
      <a class="<?= $title == 'Dashboard' ? 'active' : '' ?> btn btn-light d-flex px-5 py-3 fw-bold align-items-center rounded-0 border-5 border-bottom-0 border-top-0 border-end-0"
        href="<?= base_url('dashboard'); ?>" role="button">
        <i class="bi bi-display fs-5 d-flex align-items-center"></i>
        <p class="mb-0 ms-2 ">Dashboard</p>
      </a>
      <a class="<?= $title == 'Tables' ? 'active' : '' ?> btn btn-light d-flex px-5 py-3 fw-bold align-items-center rounded-0 border-5 border-bottom-0 border-top-0 border-end-0"
        href="<?= base_url('tables'); ?>" role="button">
        <i class="bi bi-cup fs-5 d-flex align-items-center"></i>
        <p class="mb-0 ms-2">Tables</p>
      </a>
      <a class="<?= $title == 'Orders' ? 'active' : '' ?> btn btn-light d-flex px-5 py-3 fw-bold align-items-center rounded-0 border-5 border-bottom-0 border-top-0 border-end-0"
        href="<?= base_url('orders'); ?>" role="button">
        <i class="bi bi-receipt fs-5 d-flex align-items-center"></i>
        <p class="mb-0 ms-2">Orders</p>
      </a>
      <?php /* <a class="<?= $title == 'Menu' ? 'active' : '' ?> btn btn-light d-flex px-5 py-3 fw-bold align-items-center rounded-0 border-5 border-bottom-0 border-top-0 border-end-0"
        href="<?= base_url('menu'); ?>" role="button">
        <i class="bi bi-collection fs-5 d-flex align-items-center"></i>
        <p class="mb-0 ms-2">Menu</p>
      </a>*/?>
      <div class="px-5 pb-3 border-bottom mx-4"></div>
    </div>

    <?php /* <div class="d-flex flex-column pb-4">
      <p class="px-5  mb-2 fw-light">Manage</p>
      <a class="<?= $title == 'Restaurant' ? 'active' : '' ?> btn btn-light d-flex px-5 py-3 fw-bold align-items-center rounded-0 border-5 border-bottom-0 border-top-0 border-end-0"
        href="<?= base_url('manage/restaurant'); ?>" role="button">
        <i class="bi bi-house fs-5 d-flex align-items-center"></i>
        <p class="mb-0 ms-2 ">Restaurant</p>
      </a>
      <a class="<?= $title == 'Staff' ? 'active' : '' ?> btn btn-light d-flex px-5 py-3 fw-bold align-items-center rounded-0 border-5 border-bottom-0 border-top-0 border-end-0"
        href="<?= base_url('manage/staff'); ?>" role="button">
        <i class="bi bi-people fs-5 d-flex align-items-center"></i>
        <p class="mb-0 ms-2">Staff</p>
      </a>
      <div class="px-5 pb-3 border-bottom mx-4"></div>
    </div> */?>

    <div class="d-flex flex-column">
      <p class="px-5 mb-2 fw-light">Other</p>
      <a class="<?= $title == 'Settings' ? 'active' : '' ?> btn btn-danger d-flex px-5 py-3 fw-bold align-items-center rounded-0 border-5 border-bottom-0 border-top-0 border-end-0"
        href="<?= base_url('logout'); ?>" role="button">
        <i class="bi bi-box-arrow-right fs-5 d-flex align-items-center"></i>
        <p class="mb-0 ms-2">Log Out</p>
      </a>
      
    </div>
  </div>

  <div class="d-flex flex-column vh-100 w-100" style="padding-left: 250px">
    <header class="d-flex justify-content-between align-items-center px-4" style="min-height: 100px">
    <div class='d-flex ms-4 mt-3'>
    <img src="<?= base_url(session()->get('resto_path')); ?>"
         class="img-fluid me-3 object-fit-contain" style="width: 75px; height: 75px" alt="...">
      <h1 class="mb-0 mt-3"><?= esc(session()->get('resto')) ?></h1>
    </div>
    
      <div class="d-flex">
        <a href=""
          class="btn btn-light rounded-5 border-0 me-4 shadow d-flex align-items-center justify-content-center"
          style="width: 50px; height: 50px" role="button">
          <i class="bi bi-bell-fill fs-5 d-flex align-items-center" style="color: #1E5A7C"></i>
        </a>
        <a class="<?= $title == 'Profile' ? 'active' : '' ?> btn btn-light rounded-5 d-flex align-items-center justify-content-between shadow border-0 p-0"
          href="<?= base_url('profile/' . session()->get('user_id')); ?>" style="width: 250px" role="button">
          <div class="d-flex align-items-center justify-content-center rounded-5"
            style="width: 50px; height: 50px; background-color: #1E5A7C;">
            <i class="bi bi-person-fill fs-4 d-flex align-items-center" style="color: #E2C691"></i>
          </div>

          <p class="mb-0"><?= session()->get('first_name') ?> <b><?= session()->get('last_name') ?></b></p>
          <div class="">
            <img src="<?= base_url(session()->get('image_path')); ?>"
                                    class="img-fluid rounded-5 object-fit-cover" style="width: 50px; height: 50px"
                                    alt="...">
          </div>
          <!-- <img src="..." class="rounded float-end" alt="..." style="width: 25px, height 25px"> -->
        </a>
      </div>


    </header>
    <main>
      <?= $this->renderSection('content') ?>
    </main>

  </div>







  <!-- This script includes all of Bootstrap's JavaScript-based components and behaviors, such as modal windows, dropdowns, and tooltips.  -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz"
    crossorigin="anonymous"></script>
</body>

</html>