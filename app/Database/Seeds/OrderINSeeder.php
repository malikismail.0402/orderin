<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class OrderINSeeder extends Seeder
{
    public function run()
    {

        $role_data = [
            [
                'name' => 'Admin',
            ],
            [
                'name' => 'Manager',
            ],
            [
                'name' => 'Staff',
            ],
        ];


        foreach ($role_data as $role) {
            $this->db->table('Roles')->insert($role);
        }

        $restaurant_data = [
            [
                'name' => "Betty's Burgers",
                'description' => "The Betty's Burgers is a classic burger shack, serving the freshest, juiciest and most mouth-watering burgers around.",
                'image_path' => 'restaurants/bettys-burgers.png',
                'table_num' => 8,
            ],
            [
                'name' => "Pizza Hut",
                'description' => "Pizza Hut is the largest pizza chain in the world with more than 12,000 Pizza Hut Restaurants and Delivery Units operating worldwide. Approximately 270 stores are located in Australia.",
                'image_path' => 'restaurants/pizza-hut.png',
                'table_num' => 10,
            ],
            [
                'name' => "Sushi Hub",
                'description' => "Sushi Hub opened its first store on 16 October 2006 in an alleyway in Cabramatta, NSW. It now has more than 100 stores.",
                'image_path' => 'restaurants/sushi-hub.png',
                'table_num' => 5,
            ],
        ];

        foreach ($restaurant_data as $restaurant) {
            $this->db->table('Restaurants')->insert($restaurant);
            $restoId = $this->db->insertID();
            for ($i = 1; $i < $restaurant['table_num'] + 1; $i++) {
                $this->db->table('Tables')->insert([
                    'table_id' => $i,
                    'restaurant_id' => $restoId,
                    'status' => 'inactive',
                    'price' => 0,
                ], );
            }
        }

        $user_data = [
            [
                'first_name' => 'Admin',
                'last_name' => '',
                'image_path' => 'users/default.jpeg',
                'email' => 'malikismail.0402@gmail.com',
                'phone' => '123-456-2232',
                'role_id' => 1,
                'restaurant_id' => 0,
                'password_hash' => password_hash('admin', PASSWORD_DEFAULT),
            ],
            [
                'first_name' => 'Malik',
                'last_name' => 'Zaidan',
                'image_path' => 'users/malikzaidan.jpeg',
                'email' => 'maliklego@gmail.com',
                'phone' => '123-456-2232',
                'role_id' => 2,
                'restaurant_id' => 3,
                'password_hash' => password_hash('malikmalik', PASSWORD_DEFAULT),
            ],
            [
                'first_name' => 'Ainul',
                'last_name' => 'Malik',
                'image_path' => 'users/ainulmalik.JPG',
                'email' => 'ainul.malik@ui.ac.id',
                'phone' => '123-456-2232',
                'role_id' => 3,
                'restaurant_id' => 1,
                'password_hash' => password_hash('ainulainul', PASSWORD_DEFAULT),
            ],
            [
                'first_name' => 'Malik',
                'last_name' => 'Betty',
                'image_path' => 'users/malikbetty.JPG',
                'email' => 'malikbetty@gmail.com',
                'phone' => '123-456-2232',
                'role_id' => 2,
                'restaurant_id' => 1,
                'password_hash' => password_hash('password123', PASSWORD_DEFAULT),
            ],
            [
                'first_name' => 'Malik',
                'last_name' => 'Pisa',
                'image_path' => 'users/malikpisa.JPG',
                'email' => 'malikpisa@gmail.com',
                'phone' => '123-456-2232',
                'role_id' => 2,
                'restaurant_id' => 2,
                'password_hash' => password_hash('password123', PASSWORD_DEFAULT),
            ],
            [
                'first_name' => 'INFS',
                'last_name' => 'Marker',
                'image_path' => 'users/default.jpeg',
                'email' => 'infsmarker@gmail.com',
                'phone' => '123-456-2232',
                'role_id' => 1,
                'restaurant_id' => 1,
                'password_hash' => password_hash('password123', PASSWORD_DEFAULT),
            ],
        ];


        foreach ($user_data as $user) {
            $this->db->table('Users')->insert($user);
        }


        $menu_data = [
            [
                'restaurant_id' => 1,
            ],
            [
                'restaurant_id' => 2,
            ],
            [
                'restaurant_id' => 3,
            ],
        ];


        foreach ($menu_data as $menu) {
            $this->db->table('Menus')->insert($menu);
        }

        $menu_section_data = [
            [
                'menu_section_id' => 1,
                'menu_id' => 1,
                'name' => 'Burgers',
            ],
            [
                'menu_section_id' => 2,
                'menu_id' => 1,
                'name' => 'Sides',
            ],
            [
                'menu_section_id' => 3,
                'menu_id' => 1,
                'name' => 'Drinks',
            ],
            [
                'menu_section_id' => 1,
                'menu_id' => 2,
                'name' => 'Pizza',
            ],
            [
                'menu_section_id' => 2,
                'menu_id' => 2,
                'name' => 'Pasta',
            ],
            [
                'menu_section_id' => 3,
                'menu_id' => 2,
                'name' => 'Drinks',
            ],
            [
                'menu_section_id' => 1,
                'menu_id' => 3,
                'name' => 'Maki Roll',
            ],
            [
                'menu_section_id' => 2,
                'menu_id' => 3,
                'name' => 'Nigiri',
            ],
            [
                'menu_section_id' => 3,
                'menu_id' => 3,
                'name' => 'Sashimi',
            ],
        ];


        foreach ($menu_section_data as $menu_section) {
            $this->db->table('MenuSections')->insert($menu_section);
        }

        $order_data = [
            [
                'order_id' => 1,
                'restaurant_id' => 1,
                'table_id' => 1,
                'status' => 'completed',
                'price' => 23.0
            ],
            [
                'order_id' => 2,
                'restaurant_id' => 1,
                'table_id' => 1,
                'status' => 'incoming',
                'price' => 18.5
            ],
            [
                'order_id' => 3,
                'restaurant_id' => 1,
                'table_id' => 1,
                'status' => 'processing',
                'price' => 32.0
            ],
            [
                'order_id' => 4,
                'restaurant_id' => 1,
                'table_id' => 3,
                'status' => 'incoming',
                'price' => 13.5
            ],
        ];


        foreach ($order_data as $order) {
            $this->db->table('Orders')->insert($order);
            $this->db->table('Tables')->where('table_id', $order['table_id'])->where('restaurant_id', $order['restaurant_id'])->update(['status' => 'active']);
        }

        $menu_item_data = [
            [
                'menu_id' => 1,
                'name' => "Betty's Burger",
                'image_path' => 'menu-items/bettys-burger.jpeg',
                'description' => 'Delicious Burger',
                'price' => 13.50,
                'menu_section_id' => 1
            ],
            [
                'menu_id' => 1,
                'name' => "Betty's Fries",
                'image_path' => 'menu-items/bettys-fries.jpeg',
                'description' => 'Delicious Fries',
                'price' => 4.50,
                'menu_section_id' => 2
            ],
            [
                'menu_id' => 1,
                'name' => "Betty's Cola",
                'image_path' => 'menu-items/cola.webp',
                'description' => 'Delicious Soft Drink',
                'price' => 5.00,
                'menu_section_id' => 3
            ],
            [
                'menu_id' => 2,
                'name' => "Meat Lovers Pizza",
                'image_path' => 'menu-items/meat-lovers.png',
                'description' => 'Delicious Pizza',
                'price' => 12.50,
                'menu_section_id' => 1
            ],
            [
                'menu_id' => 2,
                'name' => "Fettuccini Carbonara",
                'image_path' => 'menu-items/fettuccini.png',
                'description' => 'Delicious Pasta',
                'price' => 11.75,
                'menu_section_id' => 2
            ],
            [
                'menu_id' => 2,
                'name' => "Coca Cola",
                'image_path' => 'menu-items/cola.webp',
                'description' => 'Delicious Cola',
                'price' => 4.50,
                'menu_section_id' => 3
            ],
            [
                'menu_id' => 3,
                'name' => "Crispy Chicken Avocado Roll",
                'image_path' => 'menu-items/chicken-avo.jpeg',
                'description' => 'Delicious Sushi Roll',
                'price' => 4.00,
                'menu_section_id' => 1
            ],
            [
                'menu_id' => 3,
                'name' => "Salmon Nigiri",
                'image_path' => 'menu-items/nigiri.webp',
                'description' => 'Delicious Nigiri',
                'price' => 4.50,
                'menu_section_id' => 2
            ],
            [
                'menu_id' => 3,
                'name' => "Salmon Sashimi",
                'image_path' => 'menu-items/sashimi.webp',
                'description' => 'Delicious Sashimi',
                'price' => 3.50,
                'menu_section_id' => 3
            ],
        ];


        foreach ($menu_item_data as $menu_item) {
            $this->db->table('MenuItems')->insert($menu_item);
        }

        $order_item_data = [
            [
                'menu_item_id' => 1,
                'order_id' => 1,
                'restaurant_id' => 1,
                'amount' => 1
            ],
            [
                'menu_item_id' => 2,
                'order_id' => 1,
                'restaurant_id' => 1,
                'amount' => 1
            ],
            [
                'menu_item_id' => 3,
                'order_id' => 1,
                'restaurant_id' => 1,
                'amount' => 1
            ],
            [
                'menu_item_id' => 1,
                'order_id' => 2,
                'restaurant_id' => 1,
                'amount' => 1
            ],
            [
                'menu_item_id' => 3,
                'order_id' => 2,
                'restaurant_id' => 1,
                'amount' => 1
            ],
            [
                'menu_item_id' => 1,
                'order_id' => 3,
                'restaurant_id' => 1,
                'amount' => 2
            ],
            [
                'menu_item_id' => 3,
                'order_id' => 3,
                'restaurant_id' => 1,
                'amount' => 1
            ],
            [
                'menu_item_id' => 2,
                'order_id' => 4,
                'restaurant_id' => 1,
                'amount' => 3
            ],
        ];


        foreach ($order_item_data as $order_item) {
            $this->db->table('OrderItems')->insert($order_item);
        }

    }
}
