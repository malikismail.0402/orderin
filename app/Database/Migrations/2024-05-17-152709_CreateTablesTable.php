<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateTablesTable extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'table_id' => [
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
            ],
            'restaurant_id' => [
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
            ],
            'status' => [
                'type' => 'VARCHAR',
                'constraint' => '100',
                'default' => 'inactive',
            ],
            'price' => [
                'type' => 'FLOAT',
                'constraint' => 11,
                'unsigned' => TRUE,
            ],
            'created_at datetime default current_timestamp',
            'updated_at datetime default current_timestamp on update current_timestamp',
        ]);

        $this->forge->addKey(['table_id', 'restaurant_id'], TRUE);
        $this->forge->addForeignKey('restaurant_id', 'Restaurants', 'restaurant_id', 'CASCADE', 'CASCADE');
        $this->forge->createTable('Tables');
    }

    public function down()
    {
        $this->forge->dropTable('Tables');
    }
}
