<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateMenuSectionsTable extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'menu_section_id' => [
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
            ],
            'menu_id' => [
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
            ],
            'name' => [
                'type' => 'VARCHAR',
                'constraint' => '255',
            ],
        ]);

        $this->forge->addKey(['menu_section_id', 'menu_id'], TRUE);
        $this->forge->addForeignKey('menu_id', 'Menus', 'menu_id', 'CASCADE', 'CASCADE');
        $this->forge->createTable('MenuSections');
    }

    public function down()
    {
        $this->forge->dropTable('MenuSections');
    }
}
