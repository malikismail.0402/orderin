<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateMenusTable extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'menu_id' => [
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ],
            'restaurant_id' => [
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
            ],
            'created_at datetime default current_timestamp',
            'updated_at datetime default current_timestamp on update current_timestamp',
        ]);

        $this->forge->addKey('menu_id', TRUE);
        $this->forge->addForeignKey('restaurant_id', 'Restaurants', 'restaurant_id', 'CASCADE', 'CASCADE');
        $this->forge->createTable('Menus');
    }

    public function down()
    {
        $this->forge->dropTable('Menus');
    }
}
