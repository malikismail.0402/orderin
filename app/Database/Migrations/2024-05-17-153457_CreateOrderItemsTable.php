<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateOrderItemsTable extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'order_item_id' => [
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ],
            'menu_item_id' => [
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
            ],
            'order_id' => [
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
            ],
            'restaurant_id' => [
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
            ],
            'amount' => [
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
            ],
        ]);

        $this->forge->addKey('order_item_id', TRUE);
        $this->forge->addForeignKey(['order_id', 'restaurant_id'], 'Orders', ['order_id', 'restaurant_id'], 'CASCADE', 'CASCADE');
        $this->forge->addForeignKey('menu_item_id', 'MenuItems', 'menu_item_id', 'CASCADE', 'CASCADE');
        $this->forge->createTable('OrderItems');
    }

    public function down()
    {
        $this->forge->dropTable('OrderItems');
    }
}
