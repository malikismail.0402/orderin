<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateOrdersTable extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'order_id' => [
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
            ],
            'table_id' => [
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
            ],
            'restaurant_id' => [
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
            ],
            'status' => [
                'type' => 'VARCHAR',
                'constraint' => '255',
            ],
            'price' => [
                'type' => 'FLOAT',
                'constraint' => 11,
                'unsigned' => TRUE,
            ],
            'created_at datetime default current_timestamp',
            'updated_at datetime default current_timestamp on update current_timestamp',
        ]);

        $this->forge->addKey(['order_id', 'restaurant_id'], TRUE);
        $this->forge->addForeignKey(['table_id', 'restaurant_id'], 'Tables', ['table_id', 'restaurant_id'], 'CASCADE', 'CASCADE');
        $this->forge->createTable('Orders');
    }

    public function down()
    {
        $this->forge->dropTable('Orders');
    }
}
