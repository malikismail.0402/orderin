<?php

namespace App\Models;

use CodeIgniter\Model;

class OrderItemsModel extends Model
{
    protected $table = 'OrderItems';
    protected $primaryKey = 'order_item_id';
    protected $allowedFields = ['menu_item_id', 'order_id', 'restaurant_id'];
    protected $returnType = 'array';
    protected $useTimestamps = false;
}
