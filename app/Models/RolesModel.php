<?php

namespace App\Models;

use CodeIgniter\Model;

class RolesModel extends Model
{
    protected $table = 'Roles';
    protected $primaryKey = 'role_id';
    protected $allowedFields = ['name'];
    protected $returnType = 'array';
    protected $useTimestamps = false;
}
