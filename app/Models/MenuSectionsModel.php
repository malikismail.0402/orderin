<?php

namespace App\Models;

use CodeIgniter\Model;

class MenuSectionsModel extends Model
{
    protected $table = 'MenuSections';
    protected $primaryKey = ['menu_section_id', 'menu_id'];
    protected $allowedFields = ['name'];
    protected $returnType = 'array';
    protected $useTimestamps = false;
}
