<?php

namespace App\Models;

use CodeIgniter\Model;

class RestaurantsModel extends Model
{
    protected $table = 'Restaurants';
    protected $primaryKey = 'restaurant_id';
    protected $allowedFields = ['name', 'description', 'image_path', 'table_num'];
    protected $returnType = 'array';
    protected $useTimestamps = true;
}
