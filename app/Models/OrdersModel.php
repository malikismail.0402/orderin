<?php

namespace App\Models;

use CodeIgniter\Model;

class OrdersModel extends Model
{
    protected $table = 'Orders';
    protected $primaryKey = ['order_id', 'restaurant_id'];
    protected $allowedFields = ['status', 'table_id'];
    protected $returnType = 'array';
    protected $useTimestamps = true;
}
