<?php

namespace App\Models;

use CodeIgniter\Model;

class TablesModel extends Model
{
    protected $table = 'Tables';
    protected $primaryKey = ['table_id', 'restaurant_id'];
    protected $allowedFields = ['table_id', 'restaurant_id', 'status', 'price'];
    protected $returnType = 'array';
    protected $useTimestamps = true;
}
