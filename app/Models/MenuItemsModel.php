<?php

namespace App\Models;

use CodeIgniter\Model;

class MenuItemsModel extends Model
{
    protected $table = 'MenuItems';
    protected $primaryKey = 'menu_item_id';
    protected $allowedFields = ['name', 'description', 'image_path', 'price', 'menu_section_id', 'menu_id'];
    protected $returnType = 'array';
    protected $useTimestamps = true;
}
