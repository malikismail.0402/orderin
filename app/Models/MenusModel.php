<?php

namespace App\Models;

use CodeIgniter\Model;

class MenusModel extends Model
{
    protected $table = 'Menus';
    protected $primaryKey = 'menu_id';
    protected $allowedFields = ['restaurant_id'];
    protected $returnType = 'array';
    protected $useTimestamps = true;
}
