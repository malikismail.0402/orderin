<?php

use CodeIgniter\Router\RouteCollection;

/**
 * @var RouteCollection $routes
 */
$routes->get('/', 'orderINController::index');

$routes->match(['GET', 'POST'], '/login', 'Auth::login');
$routes->get('/login/google', 'Auth::google_login');
$routes->get('/login/callback', 'Auth::google_callback');
$routes->match(['GET', 'POST'], '/register', 'orderINController::register');
$routes->match(['GET', 'POST'], '/register-restaurant', 'orderINController::register_restaurant');
$routes->get('/logout', 'Auth::logout');

$routes->get('/order/(:num)/(:num)', 'orderINController::order/$1/$2');

$routes->group('', ['filter' => 'login'], function($routes) {
    $routes->get('/dashboard', 'orderINController::dashboard');
    $routes->get('/tables', 'orderINController::tables');
    $routes->get('/tables/(:num)', 'orderINController::table_details/$1');
    $routes->get('/orders', 'orderINController::orders');
    $routes->get('/orders/(:num)', 'orderINController::order_details/$1');
    $routes->get('/menu', 'orderINController::menu');
    $routes->get('/settings', 'orderINController::settings');
    
    $routes->get('/profile/(:any)', 'orderINController::profile/$1');
    
    $routes->get('/manage/restaurant', 'orderINController::restaurant');
    $routes->get('/manage/staff', 'orderINController::staff');
});

$routes->group('admin', ['filter' => 'admin'], function($routes) {
    $routes->get('', 'orderINController::admin');
    $routes->get('restaurants', 'orderINController::admin_restaurants');
    $routes->get('users', 'orderINController::admin_users');
    $routes->get('profile', 'orderINController::admin_profile');
});

